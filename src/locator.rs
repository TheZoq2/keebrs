#[rustfmt::skip]
use alloc::vec;
use alloc::{
    string::String,
    vec::Vec,
};

use crate::{
    key::KeyPos,
    vecmap::VecMap,
};

pub struct Locator {
    map: Vec<String>,

    // mapping of position in `map` to actual board position
    assignments: Vec<Option<i8>>,
    // mapping of actual board position to position in the map
    boards: VecMap<i8, u8>,
}

impl Locator {
    pub fn new(map: &[&str]) -> Self {
        Locator {
            map: map.iter().map(|s| (*s).into()).collect(),
            assignments: vec![None; map.len()],
            boards: Default::default(),
        }
    }

    pub fn add_board(&mut self, mut real_pos: i8, board_type: String) {
        // If there was another board here before and a new one is taking its place,
        // clear the old entry
        if let Some(prev) = self.boards.take(&real_pos) {
            self.assignments[prev as usize] = None;
        }
        let mut offset = 0;
        loop {
            if offset >= self.map.len() {
                return;
            }
            // Find the index of the first `board_type` in the map
            if let Some(pos) = self.map[offset..]
                .iter()
                .position(|t| t == &board_type)
                .map(|pos| pos + offset)
            {
                if let Some(current) = self.assignments[pos] {
                    // If there's already a board in that assignment, check to
                    // see if its absolute position is to the left of this one
                    if current > real_pos {
                        // if the new board is to the left of the existing one,
                        // replace it and find a new slot for the previous one.
                        self.assignments[pos] = Some(real_pos);
                        self.boards.take(&current);
                        self.boards.insert(real_pos, pos as u8);
                        real_pos = current;
                    }
                    offset = pos + 1;
                } else {
                    // If there's nothing there, set it to the new board
                    self.assignments[pos] = Some(real_pos);
                    self.boards.insert(real_pos, pos as u8);
                    return;
                }
            } else {
                // If there aren't any instances of `board_type`, don't assign
                // this one to anything
                return;
            }
        }
    }

    /// Find the "real" mapping for the given key
    ///
    /// If no board has been assigned for the key's `z` value, returns `None`
    pub fn remap(&self, key: KeyPos) -> Option<KeyPos> {
        self.boards.get(&(key.z as _)).map(|newpos| KeyPos {
            z: *newpos as _,
            ..key
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_locator() {
        let mut locator = Locator::new(&["ortho5x7", "ortho5x7"]);
        locator.add_board(0, "ortho5x7".into());
        locator.add_board(-1, "ortho5x7".into());

        assert_eq!(
            Some(KeyPos { x: 0, y: 0, z: 0 }),
            locator.remap(KeyPos { x: 0, y: 0, z: -1 })
        );
    }
}
