//! Core routines for keyboard operation

use alloc::string::String;

use futures::{
    future::{
        select,
        Either,
    },
    prelude::*,
};

use crate::{
    locator::Locator,
    serial::Msg,
    stateful::{
        ScanStateful,
        ScanStatefulExt,
    },
    translate::{
        Translate,
        Translated,
        Translator,
    },
};

/// The role of one board in a chain
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Role {
    /// This board owns the USB connection
    Center,
    /// This board is on the left
    Left,
    /// This board is on the right
    Right,
}

/// The core keyboard manager
pub struct Core<T, R> {
    role: Role,
    my_type: String,
    translator: Translator,
    locator: Locator,
    msg_tx: T,
    msg_rx: R,
    extra_msg_tx: T,
}

impl<T, R> Core<T, R>
where
    T: Sink<Msg, Error = !> + Clone + Unpin,
    R: Stream<Item = Msg> + Unpin,
{
    /// Create a new keyboard core
    pub fn new(
        my_type: &str,
        board_types: &[&str],
        translator: Translator,
        (msg_tx, msg_rx): (T, R),
        extra_msg_tx: T
    ) -> Core<T, R> {
        Core {
            role: Role::Center,
            my_type: my_type.into(),
            translator,
            locator: Locator::new(board_types),
            msg_tx,
            msg_rx,
            extra_msg_tx,
        }
    }

    /// Using the three futures, select the role based on whichever resolves first
    #[embrio_async::embrio_async]
    pub async fn choose_role<U, LF, RF>(&'future mut self, usb: U, left: LF, right: RF) -> Role
    where
        LF: Future + Unpin,
        RF: Future + Unpin,
        U: Future + Unpin,
    {
        let msg = select(left, right);
        let role = match select(usb, msg).await {
            Either::Left(_) => Role::Center,
            Either::Right((Either::Left(_), _)) => Role::Right,
            Either::Right((Either::Right(_), _)) => Role::Left,
        };
        self.role = role;
        role
    }

    /// Read from the downstream port, perform the proper adjustments to the z value,
    /// and forward the messages along the sink
    #[embrio_async::embrio_async]
    pub fn consume_from<M>(&mut self, mut msgs: M) -> impl Future<Output = ()>
    where
        M: Stream<Item = Msg> + Unpin,
    {
        let mut sink = self.msg_tx.clone();
        async move {
            while let Some(msg) = msgs.next().await {
                sink.send(msg).await.unwrap();
            }
            panic!("no more messages, should never get here");
        }
    }

    /// Run the scanner on every item from the timer stream and forward the key
    /// events along the sink.
    #[embrio_async::embrio_async]
    pub fn run_scanner<Ti, Sc>(
        &mut self,
        mut timer: Ti,
        mut scanner: Sc,
    ) -> impl Future<Output = ()>
    where
        Ti: Stream + Unpin,
        Sc: ScanStateful + Unpin,
    {
        let mut output = self.msg_tx.clone();
        let mut extra_output = self.extra_msg_tx.clone();
        let role = self.role;
        let board_type = self.my_type.clone();
        async move {
            output.send(Msg::BoardType(0, board_type)).await.unwrap();
            while let Some(_) = timer.next().await {
                let msg = scanner.change().await.map(Msg::KeyEvent);
                if let Some(msg) = msg {
                    extra_output.send(msg.clone()).await.unwrap();
                    output.send(msg).await.unwrap();
                } else if role == Role::Center {
                    output.send(Msg::Tick).await.unwrap();
                }
            }
            panic!("no more ticks, should never get here");
        }
    }

    /// Forward all events via the provided sink
    ///
    /// Applies an offset to the "z" component of the key position
    #[embrio_async::embrio_async]
    pub async fn forward_to<W>(&'future mut self, mut sink: W)
    where
        W: Sink<Msg, Error = ()> + Unpin,
    {
        let offset = match self.role {
            Role::Center => 0,
            Role::Left => -1,
            Role::Right => 1,
        };
        while let Some(mut msg) = self.msg_rx.next().await {
            match msg {
                Msg::KeyEvent(ref mut key) => {
                    key.pos.z += offset;
                }
                Msg::BoardType(ref mut pos, _) => {
                    *pos += offset;
                }
                _ => {}
            }
            let _ = sink.send(msg).await;
        }
        panic!("no more messages, should never get here");
    }

    /// Translate all aggregated events and send them via the provided sink
    #[embrio_async::embrio_async]
    pub async fn translate_to<W>(&'future mut self, mut sink: W)
    where
        W: Sink<Translated, Error = ()> + Unpin,
    {
        while let Some(msg) = self.msg_rx.next().await {
            let mut key = match msg {
                Msg::KeyEvent(key) => Some(key),
                Msg::BoardType(pos, board_type) => {
                    self.locator.add_board(pos, board_type);
                    continue;
                }
                Msg::Tick => None,
                _ => continue,
            };
            if let Some(ref mut key) = key {
                if let Some(newpos) = self.locator.remap(key.pos) {
                    key.pos = newpos;
                } else {
                    continue;
                }
            }
            if let Some(translated) = self.translator.translate(key) {
                let _ = sink.send(translated).await;
            }
        }
        panic!("no more messages, should never get here");
    }

}
