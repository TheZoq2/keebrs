#![allow(dead_code)]

use core::borrow::{
    Borrow,
    BorrowMut,
};

use alloc::vec::Vec;

// Simple linear map
//
// Lookups, insertions, and removals are all O(n).
// Mainly optimized for code size since BTreeMap is
// rather large.
#[derive(Debug, Clone)]
pub struct VecMap<K, V>(Vec<Option<(K, V)>>);

impl<K, V> Default for VecMap<K, V>
where
    K: Eq,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<K, V> VecMap<K, V>
where
    K: Eq,
{
    pub fn new() -> Self {
        VecMap(Default::default())
    }

    pub fn get<T>(&self, key: &T) -> Option<&V>
    where
        K: Borrow<T>,
        T: Eq,
    {
        self.0
            .iter()
            .filter_map(|cell| {
                if let Some((cellkey, value)) = cell {
                    if key == cellkey.borrow() {
                        return Some(value);
                    }
                }
                None
            })
            .nth(0)
    }
    pub fn get_mut<T>(&mut self, key: &T) -> Option<&mut V>
    where
        K: BorrowMut<T>,
        T: Eq,
    {
        self.0
            .iter_mut()
            .filter_map(|cell| {
                if let Some((cellkey, value)) = cell {
                    if key == cellkey.borrow_mut() {
                        return Some(value);
                    }
                }
                None
            })
            .nth(0)
    }

    pub fn take<T>(&mut self, key: &T) -> Option<V>
    where
        K: Borrow<T>,
        T: Eq,
    {
        self.0
            .iter_mut()
            .find(|cell| {
                if let Some((cellkey, _)) = cell {
                    key == cellkey.borrow()
                } else {
                    false
                }
            })
            .and_then(|cell| cell.take())
            .map(|(_, value)| value)
    }

    pub fn insert(&mut self, key: K, value: V) {
        if let Some(cell) = self.0.iter_mut().find(|cell| cell.is_none()) {
            cell.replace((key, value));
        } else {
            self.0.push(Some((key, value)))
        }
    }

    pub fn inner(&self) -> &Vec<Option<(K, V)>> {
        &self.0
    }
}
