//! Abstract keyboard types and traits

#![cfg_attr(not(test), no_std)]
#![warn(missing_docs)]
#![feature(never_type, generators)]

extern crate alloc;

#[allow(unused_macros)]
macro_rules! dbg {
    ($($t:tt)*) => ({
        #[cfg(test)]
        {
            std::dbg!($($t)*)
        }
        #[cfg(not(test))]
        {
            $($t)*
        }
    })
}

mod locator;
mod vecmap;

pub mod core;
pub mod debounce;
pub mod key;
pub mod keycode;
pub mod led;
pub mod matrix;
pub mod serial;
pub mod stateful;
pub mod translate;

// TODO: Document USB stuff
#[allow(missing_docs)]
pub mod usb;
