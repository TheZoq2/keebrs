//! Phyiscal key attributes

use serde::{
    Deserialize,
    Serialize,
};

use newtype::NewType;

use crate::keycode::KeyCode;

/// The matrix position of a key
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Copy, Ord, PartialOrd)]
pub struct KeyPos {
    /// The "pulled" line of the key
    pub x: u8,
    /// The "read" line of the key
    pub y: u8,
    /// The matrix the key belongs to
    ///
    /// 0 is the "center" matrix, left matrices are < 0 , right > 0
    pub z: i8,
}

/// The state of a key
///
/// `true` == "pressed"
///
/// `false` == "not pressed"
#[derive(Serialize, Deserialize, NewType, Debug, PartialEq, Eq, Clone, Copy)]
pub struct KeyState(pub bool);

/// A physical key's position and state
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Copy)]
pub struct PhysKey {
    /// The position of the key
    pub pos: KeyPos,
    /// The state of the key
    pub state: KeyState,
}

/// A resolved key with a KeyCode
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Copy)]
pub struct CodeKey {
    /// The actual keycode
    pub code: KeyCode,

    /// The state of the key
    pub state: KeyState,
}
