//! Utilities for sending keeb info over serial

#![allow(clippy::cast_lossless)]

#[cfg(test)]
mod test;

use alloc::string::String;

use serde::{
    Deserialize,
    Serialize,
};

use futures::prelude::*;

use embrio::io::{
    Read,
    Write,
};

use async_codec::{
    Decode,
    DecodeResult,
    Encode,
    EncodeResult,
    Framed,
    ReadFrameError,
    WriteFrameError,
};

use crate::key::PhysKey;

const CHAN_BUF: usize = 32;

/// Encoder/Decoder for [Msg]s
pub struct MsgCodec;

impl Encode for MsgCodec {
    type Item = Msg;
    type Error = postcard::Error;

    fn encode(&mut self, item: &Msg, buf: &mut [u8]) -> EncodeResult<Self::Error> {
        match postcard::to_slice_cobs(item, buf) {
            Ok(buf) => Ok(buf.len()).into(),
            Err(postcard::Error::SerializeBufferFull) => EncodeResult::Overflow(0),
            Err(e) => Err(e).into(),
        }
    }
}

impl Decode for MsgCodec {
    type Item = Msg;
    type Error = postcard::Error;

    fn decode(&mut self, buf: &mut [u8]) -> (usize, DecodeResult<Msg, postcard::Error>) {
        let end = match buf.iter().position(|b| *b == 0x00) {
            Some(pos) => pos + 1,
            None => return (0, DecodeResult::UnexpectedEnd),
        };

        (end, postcard::from_bytes_cobs(&mut buf[..end]).into())
    }
}

/// A message to be sent over a serial port
#[derive(Deserialize, Serialize, Debug, Clone, Eq, PartialEq)]
pub enum Msg {
    /// "Hello" message sent at bootup from the master controller to the others
    Hello,
    /// Board type announcement from a child board
    BoardType(i8, String),
    /// Physical key press/release event from a child board to the master
    /// controller.
    KeyEvent(PhysKey),
    /// A scanner tick without a key event
    ///
    /// This should never be sent by Left/Right boards
    Tick,
}

#[derive(Default)]
struct Buf([u8; CHAN_BUF]);

impl AsRef<[u8]> for Buf {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}
impl AsMut<[u8]> for Buf {
    fn as_mut(&mut self) -> &mut [u8] {
        &mut self.0
    }
}

impl Msg {
    /// Wrap a [Read] to create a stream of messages
    pub fn stream<S: Read>(
        stream: S,
    ) -> impl Stream<Item = Result<Self, ReadFrameError<S::Error, postcard::Error>>> {
        Framed::new(stream, MsgCodec, Buf::default())
    }

    /// Wrap a [Write] to create a sink for messages
    pub fn sink<S: Write>(
        stream: S,
    ) -> impl Sink<Msg, Error = WriteFrameError<S::Error, postcard::Error>> {
        Framed::new(stream, MsgCodec, Buf::default())
    }
}
