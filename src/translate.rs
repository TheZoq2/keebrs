//! Utilities for translating physical events to logical events

use alloc::collections::VecDeque;

use crate::{
    key::{
        CodeKey,
        KeyPos,
        KeyState,
        PhysKey,
    },
    keycode::KeyCode,
    vecmap::VecMap,
};

/// Multiple simultaneous keypresses
///
/// All pressed and released simultaneously
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub struct MultiKey {
    /// The keycodes pressed/released
    pub codes: &'static [KeyCode],
    /// Pressed or released
    pub state: KeyState,
}

/// The result of a translation
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Translated {
    /// A single key event
    Single(CodeKey),
    /// Multiple key events
    Multi(MultiKey),
}

/// Trait for translating a physical key event to a keycode event
pub trait Translate {
    /// (Maybe) Translate a keystroke to a keycode
    ///
    /// This should be called upon every scan, whether a key event occurred or
    /// not, as well as for every event sent from another board. The "no event"
    /// calls should be used by the translator to judge the passage of time and
    /// as a mechanism to send multiple keys "at the same time."
    fn translate(&mut self, key: Option<PhysKey>) -> Option<Translated>;
}

/// Keymap action
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Action {
    /// No action
    Nop,
    /// Transparent: Defers to the layer below
    Trans,
    /// Regular keypress
    Key(KeyCode),
    /// Sequence of keypresses. Sent after release
    Seq(&'static [Action]),
    /// Combination of keys. All pressed and released together
    Combo(&'static [KeyCode]),
    /// Activate a layer
    Layer(usize),
    /// Function call
    Fn(fn()),
}

/// A single keymap
pub struct Map(pub &'static [&'static [Action]]);

/// A single key matrix
///
/// This represents a single board
pub struct Board {
    /// The layers of this segment
    pub layers: &'static [Map],
    /// The orientation of scanning
    pub orientation: Orientation,
}

/// The orientation of the board
pub enum Orientation {
    /// Rows then columns
    RC,
    /// Columns then rows
    CR,
}

/// A full layout
pub struct Layout {
    /// The actual layers comprising the full layout
    pub boards: &'static [Board],
}

/// A key translator backed by a `Layout`
pub struct Translator {
    layout: Layout,
    presses: VecMap<KeyPos, Action>,
    layers: u32,
    queue: VecDeque<Translated>,
    full: usize,
    bucket: usize,
}

impl Translator {
    /// Initialize a new `Tranlator`
    pub fn new(layout: Layout, poll_rate: u32) -> Self {
        let full = (poll_rate / 1000) as usize * 10;
        Self {
            layout,
            presses: Default::default(),
            layers: 0x00_00_00_01,
            queue: VecDeque::new(),
            full,
            bucket: full,
        }
    }

    fn top_layer(&self) -> usize {
        31 - self.layers.leading_zeros() as usize
    }

    fn is_active(&self, layer: usize) -> bool {
        (self.layers & (0x01 << layer)) != 0
    }

    #[inline(never)]
    fn get_action(&self, layer: usize, pos: KeyPos) -> Option<Action> {
        if pos.z < 0 {
            return None;
        }
        let action = self
            .layout
            .boards
            .get(pos.z as usize)
            .and_then(|board| {
                let (outer, inner) = match board.orientation {
                    Orientation::RC => (pos.x, pos.y),
                    Orientation::CR => (pos.y, pos.x),
                };
                board
                    .layers
                    .get(layer)
                    .map(|matrix| ((outer as usize, inner as usize), matrix.0))
            })
            .and_then(|((outer, inner), matrix)| matrix.get(outer).and_then(|i| i.get(inner)));
        dbg!(action.cloned())
    }

    #[inline(never)]
    fn get_logical_action(&self, pos: KeyPos) -> Option<Action> {
        let mut layer = self.top_layer();
        loop {
            if self.is_active(layer) {
                let action = self.get_action(layer, pos);
                if let Some(action) = action {
                    if action != Action::Trans {
                        return Some(action);
                    }
                }
            }
            if layer == 0 {
                return None;
            }
            layer -= 1;
        }
    }

    fn enqueue(&mut self, key: Translated) {
        self.queue.push_back(key)
    }

    fn dequeue(&mut self) -> Option<Translated> {
        if self.bucket < self.full {
            self.bucket += 1;
            return None;
        }
        if !self.queue.is_empty() {
            self.bucket = 0;
        }

        dbg!(self.queue.pop_front())
    }

    fn action_down(&mut self, action: Action) {
        match action {
            Action::Fn(_) => {}
            Action::Nop => {}
            Action::Trans => unreachable!("should never return a transparent action"),
            Action::Key(code) => self.enqueue(Translated::Single(CodeKey {
                code,
                state: true.into(),
            })),
            Action::Layer(layer) => {
                self.layers ^= 0x1 << layer;
            }
            Action::Seq(_keys) => {}
            Action::Combo(codes) => self.enqueue(Translated::Multi(MultiKey {
                codes,
                state: true.into(),
            })),
        }
    }

    fn action_up(&mut self, down_action: Action, pos: KeyPos) {
        match down_action {
            Action::Fn(f) => f(),
            Action::Trans => {}
            Action::Nop => {}
            Action::Key(code) => self.enqueue(Translated::Single(CodeKey {
                code,
                state: false.into(),
            })),
            Action::Layer(layer) => {
                let layer_action = self.get_action(layer, pos);
                if layer_action.is_none() || layer_action == Some(Action::Trans) {
                    self.layers ^= 0x1 << layer;
                }
            }
            Action::Seq(actions) => {
                for action in actions {
                    self.action_down(*action);
                    self.action_up(*action, pos);
                }
            }
            Action::Combo(codes) => self.enqueue(Translated::Multi(MultiKey {
                codes,
                state: false.into(),
            })),
        }
    }

    fn get_press(&mut self, pos: KeyPos) -> Option<Action> {
        self.presses.take(&pos)
    }

    fn add_press(&mut self, pos: KeyPos, action: Action) {
        self.presses.insert(pos, action)
    }
}

impl Translate for Translator {
    fn translate(&mut self, key: Option<PhysKey>) -> Option<Translated> {
        if key.is_none() {
            return self.dequeue();
        }

        let key = key?;
        let pos = key.pos;
        let state = key.state;
        if *state {
            if let Some(action) = self.get_logical_action(pos) {
                self.action_down(action);
                self.add_press(pos, action);
            }

            self.dequeue()
        } else {
            if let Some(down_action) = self.get_press(pos) {
                self.action_up(down_action, pos);
            }

            self.dequeue()
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use Action::*;
    use KeyCode::*;

    #[rustfmt::skip]
    const TEST_LAYOUT: Layout = Layout {
        boards: &[
            Board {
                orientation: Orientation::RC,
                layers: &[
                    Map(&[
                        &[Key(Kb0), Seq(&[Combo(&[KbLShift, KbP]), Key(KbK), Key(KbB)])],
                        &[Key(KbA), Key(KbS)],
                    ]),
                    Map(&[
                        &[Key(KbQ), Seq(&[Key(KbA), Key(KbB)])],
                        &[Key(KbE), Combo(&[KbLShift, KbT])],
                    ]),
                ],
            },
            Board {
                orientation: Orientation::RC,
                layers: &[
                    Map(&[
                        &[Key(KbLShift), Key(KbRCtrl)],
                        &[Layer(1), Layer(1)],
                    ]),
                    Map(&[
                        &[Key(KbJ), Key(KbK)],
                        &[Layer(1), Trans],
                    ]),
                ],
            },
        ],
    };

    macro_rules! key_input {
        ($row:expr, $col:expr, $board:expr, nop) => {
            Option::None
        };
        ($row:expr, $col:expr, $board:expr, $press:expr) => {
            Some(PhysKey {
                pos: KeyPos {
                    x: $row,
                    y: $col,
                    z: $board,
                },
                state: $press.into(),
            })
        };
    }
    macro_rules! key_output {
        ($code:expr, $press:expr) => {
            Translated::Single(CodeKey {
                code: $code,
                state: $press.into(),
            })
        };
    }

    macro_rules! multi_output {
        ($($code:expr),* ; $press:expr) => {
            Translated::Multi(MultiKey {
                codes: &[$($code),*],
                state: $press.into(),
            })
        };
    }

    #[test]
    fn test_layout() {
        let mut translator = Translator::new(TEST_LAYOUT, 0);
        let input = &[
            key_input!(0, 0, 0, true),  // 0 down
            key_input!(0, 0, 0, false), // 0 up
            key_input!(1, 0, 1, true),  // layer 1 down
            key_input!(0, 0, 0, true),  // Q down
            key_input!(1, 0, 1, false), // layer 1 up
            key_input!(0, 0, 0, false), // Q up
            key_input!(0, 0, 0, true),  // Q down
            key_input!(0, 0, 0, false), // Q up
            key_input!(1, 0, 1, true),  // layer 1 down
            key_input!(1, 0, 1, false), // layer 1 up
            key_input!(1, 1, 1, true),  // layer 1 down
            key_input!(0, 0, 0, true),  // Q down
            key_input!(1, 1, 1, false), // layer 1 up
            key_input!(0, 0, 0, false), // Q up
            key_input!(0, 0, 0, true),  // 0 down
            key_input!(0, 0, 0, false), // 0 up
            key_input!(1, 0, 1, true),  // layer 1 down
            key_input!(1, 0, 1, false), // layer 1 up
            key_input!(0, 1, 0, true),  // a, b down
            key_input!(0, 1, 0, false), // a, b up
            key_input!(1, 0, 1, true),  // layer 1 down
            key_input!(1, 0, 1, false), // layer 1 up
            key_input!(0, 1, 0, true),
            key_input!(0, 1, 0, false),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
            key_input!(0, 1, 0, nop),
        ];
        let output: Vec<_> = input
            .iter()
            .filter_map(|e| translator.translate(*e))
            .collect();

        assert_eq!(
            &output,
            &[
                key_output!(Kb0, true),
                key_output!(Kb0, false),
                key_output!(KbQ, true),
                key_output!(KbQ, false),
                key_output!(KbQ, true),
                key_output!(KbQ, false),
                key_output!(KbQ, true),
                key_output!(KbQ, false),
                key_output!(Kb0, true),
                key_output!(Kb0, false),
                key_output!(KbA, true),
                key_output!(KbA, false),
                key_output!(KbB, true),
                key_output!(KbB, false),
                multi_output!(KbLShift, KbP; true),
                multi_output!(KbLShift, KbP; false),
                key_output!(KbK, true),
                key_output!(KbK, false),
                key_output!(KbB, true),
                key_output!(KbB, false),
            ]
        );
        assert!(translator.presses.inner().iter().all(|opt| opt.is_none()));
    }
}
