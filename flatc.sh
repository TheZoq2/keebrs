#!/bin/bash

cd "$(dirname "${BASH_SOURCE[0]}")"

git submodule update --init

cd flatbuffers
cmake .
make
cd -

# probably not going to use this for inter-board comms,
# leaving this here in case I want to use it for PC -> board stuff.

# ./flatbuffers/flatc -r -o src/serial inter_board.fbs

# for f in src/serial/*_generated.rs; do
#     mv $f $(echo $f | sed 's/_generated//g')
# done